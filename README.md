# A simple SPREFQL Demo

This demo consists of 7 SPREFQL queries, over the [LinkedMDB](http://www.linkedmdb.org/) database using [PreferS](http://bitbucket.org/dataengineering/sprefql), our prototype implementation of SPREFQL.

## Requierements
* JDK 8
* Maven 3.0.5
* Docker version 17.05.0-ce
* docker-compose ver. 1.15.0

## How to deploy
Firstly, start the Virtuoso Store that contains the LinkedMDB database:
```
$ docker-compose up -d lmdb
```
Then, build the SPREFQL implementation from  http://bitbucket.org/dataengineering/sprefql and copy the resulting *.war file into the ./prefers-init/ directory:
```
$ git clone http://bitbucket.org/dataengineering/sprefql
$ cd sprefql/prefers
$ mvn clean package
$ cd ../../
$ cp sprefql/prefers/target/prefers-1.0.war prefers-init/
```
Then, start the sprefql container:
```
$ docker-compose up -d prefers
$ docker-compose exec prefers bash -c 'cp /opt/prefers/prefers-1.0.war /usr/local/tomcat/webapps/'
$ docker-compose exec prefers bash -c 'mkdir /usr/local/tomcat/webapps/sprefql/'
$ docker-compose exec prefers bash -c 'cp /opt/prefers/index.html /usr/local/tomcat/webapps/sprefql/'
```
You can now issue queries on http://localhost:8888/prefers-1.0/repositories/sparql
```
$ curl -X POST -H "Accept: text/csv" --data-urlencode "query=
PREFIX dc: <http://purl.org/dc/terms/>
PREFIX movie: <http://data.linkedmdb.org/resource/movie/>
SELECT ?title ?genre (xsd:float(?runtimeStr) as ?runtime) 
WHERE {
 ?s a movie:film.
 ?s dc:title ?title.
 ?s movie:genre ?g.
 ?g movie:film_genre_name ?genre.
 ?s movie:runtime ?runtimeStr.
}
PREFER (?title1 ?genre1 ?runtime1) 
TO     (?title2 ?genre2 ?runtime2) 
IF (?genre1 = ?genre2 &&  ?runtime1 > ?runtime2) 
" http://localhost:8888/prefers-1.0/repositories/sparql
```
Alternatively, you can connect with your browser in the following link http://localhost:8888/sprefql

